var TelegramBot = require("node-telegram-bot-api");
var token = require("./config").token
var bot = new TelegramBot(token, {polling:true});
var request = require("request")
const Entities = require('html-entities').XmlEntities;
 const entities = new Entities();

canUse = require("./config").canUse

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function sendText(chatId,msg){
  var lines = msg.split("\n")
      var buffer = ""
      for(var i=0;i<lines.length;i++){
        if((buffer.length + lines[i].length) > 4096){
          bot.sendMessage(chatId,buffer);
          buffer = "";
          sleep(100)
        }else{
          buffer = buffer+"\n"
        }
        buffer = buffer+lines[i];
      }
      bot.sendMessage(chatId,buffer);
}


function cheatsheet(chatId,cheat){
  bot.sendMessage(chatId,"Requesting: https://cheat.sh/"+encodeURIComponent(cheat)+"?qT");
  request("https://cheat.sh/"+encodeURIComponent(cheat)+"?qT",function(error,response,body){
    var res = body.match(/\<pre\>([\S\s]+?)\n+^\$/m);
    //console.log(res[1]);
    
    if(!error && response.statusCode == 200 && res != null && res[1] != null && res[1] !== undefined){
      sendText(chatId,entities.decode(res[1]));
    }
  });
}

//no command (query)
bot.onText(/^([^\/].*)/,function(msg,match){
  if(!canUse(msg.from)){return;}
  var chatId = msg.chat.id;
  var cheat = match[1];
  cheatsheet(chatId,cheat);
});

bot.onText(/^\/list (.*)/,function(msg,match){
  if(!canUse(msg.from)){return;}
  var chatId = msg.chat.id;
  var arg = match[1];
  cheatsheet(chatId,arg+"/:list");
});

bot.onText(/^\/learn (.*)/,function(msg,match){
  if(!canUse(msg.from)){return;}
  var chatId = msg.chat.id;
  var arg = match[1];
  cheatsheet(chatId,arg+"/:learn");
});

bot.onText(/^\/helloworld (.*)/,function(msg,match){
  if(!canUse(msg.from)){return;}
  var chatId = msg.chat.id;
  var arg = match[1];
  cheatsheet(chatId,arg+"/:hello");
});

bot.onText(/^\/start/,function(msg,match){
  var chatId = msg.chat.id;
  if(canUse(msg.from)){bot.sendMessage(chatId,"Welcome to the cheat.sh bot!");}
  else{bot.sendMessage(chatId,"Sorry but this is a private bot of @Baschdel\nif you want the source code write him a nice message");}
});

bot.onText(/^\/help/,function(msg,match){
  if(!canUse(msg.from)){return;}
  var chatId = msg.chat.id;
  bot.sendMessage(chatId,"Send this bot a query to make to cheat.sh and it returns the result")
  bot.sendMessage(chatId,"for help on the cheat.sh service type ':help'")
});

bot.on('polling_error', (error) => {
  console.log(error.code);  // => 'EFATAL'
});
