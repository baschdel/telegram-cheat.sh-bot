# Telegram cheat.sh bot

A bot that querys cheat.sh with the messages you send it and gives you the result back
written with node.js

## Setup

download this repository

`npm install`

Contact @BotFather on telegram to get a bot token

rename `example_config.js` to `config.js` and edit it (file is commented)

`node index.js` to run

## Usage

Send the bot a message with the name/path of the cheat sheet and the bot returns it.

Commands:
- /help the short verion of this without the commands
- /list &gt;topic&lt; - shoutcut for &gt;topic&lt;/:lists
- /learn &gt;laguage&lt; - shoutcut for &gt;laguage&lt;/:learn
- /list &gt;laguage&lt; - shoutcut for &gt;laguage&lt;/:hello

